import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DbDemon {
	private static DbDemon db=null;
	
	protected DbDemon(){
		init();
	}
	
	public static DbDemon getDb(){
		if(db==null)
			db=new DbDemon();
		return db;
	}
	

    public void init(){
        buildTables(); 	
    }
	
    public String exec(String str){
    	Statement stmt=null;
    	String msg="Successo";
		try {
			Connection c=null;
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			stmt = c.createStatement();
			stmt.executeUpdate(str);
			stmt.close();
			c.close();
		} catch (SQLException e) {
			return "ECCEZIONE SQL, operazione non riuscita: "+e.getMessage();
		} catch (ClassNotFoundException e) {
			return "Classe non trovata. il che è strano... chiedere aiuto alle sfere celesti";
		}
		return msg;
    }
    
    public void insert(String str){
    	exec(str);
    }
    
    public void dbgClear(){
    	String purge=  "delete from artista;";
    	exec(purge);
    	purge = "drop table artista;";
    	exec(purge);
    	purge = "drop table sta_in;";
    	exec(purge);
    }
    
    public void buildTables(){
    	String make =
    			"CREATE TABLE IF NOT EXISTS artista " +
                "(nome TEXT not null, " + 
    			"cognome text not null,"
    			+ "soprannome text," +
                " nascita varchar," + 
                " morte varchar,"
                + "PRIMARY KEY(nome,cognome));";
    	exec(make);
    	make ="create table if not exists citta"
    			+ "(nomeC text primary key);";
    	exec(make);
    	make="create table if not exists sta_in"
    			+ "(nomeA text not null,"
    			+ "cognomeA not null,"
    			+ "soprannomeA,"
    			+ "da varchar ,"
    			+ "a varchar ,"
    			+ "citNome text not null,"
    			+ "primary key (nomeA,cognomeA,da,a,citNome),"
    			+ "foreign key (nomeA) references artista(nome),"
    			+ "foreign key (cognomeA) references artista(cognome),"
    			+ "foreign key (soprannomeA) references artista(soprannome),"
    			+ "foreign key (citNome) references citta(nomeC));";
    	exec(make);
    }
    
    public void dbgInsert(){
    	String m = "insert into artista (nome,cognome,soprannome,nascita,morte) "
    			+ "values('Tizio','De Tizi','Lo Tizio','1750','1820');";
    	exec(m);
    	m = "insert into artista (nome,cognome,nascita,morte) "
    			+ "values('Caio','De Cai','1750','1820');";
    	exec(m);
    	m = "insert into artista (nome,cognome,soprannome,nascita,morte) "
    			+ "values('Sempronio','De Semproni','Te l Sempronio','1750','1820');";
    	exec(m);
    }
    
    public ArrayList<ArrayList<String>> query(String str){
		Connection c=null;
    	Statement stmt=null;
    	ResultSet rs = null;
    	ArrayList<ArrayList<String>> ret = new ArrayList<ArrayList<String>>();
    	try {
    		
    		Class.forName("org.sqlite.JDBC");
    		c = DriverManager.getConnection("jdbc:sqlite:test.db");
			stmt= c.createStatement();
	    	rs = stmt.executeQuery(str);
	    	ResultSetMetaData rsmt = rs.getMetaData();
	    	int numColumns = rsmt.getColumnCount();
	    	while(rs.next()){
	    		ArrayList<String> row = new ArrayList<String>();
	    		for(int i=1; i<numColumns+1; i++){
	    			row.add(rs.getString(i));
	    		}
	    		ret.add(row);
	    	}
	    	rs.close();
	    	stmt.close();
	    	c.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
    	return ret;
    }
    
}
