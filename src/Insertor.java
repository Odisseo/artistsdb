import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class Insertor extends JPanel{
	CustomForm form;
	CityForm cityForm;
	CityForm artInCity;
	CustomForm form2;
	JPanel insertArtist;
	JPanel insertCity;
	JPanel insertArtistInCity;
	JButton BinsertArtist;
	String IGNORANZA = "     ";
	DbDemon db;
	NotificationArea notificationArea;


	public Insertor(){
		db = DbDemon.getDb();
		System.out.println(db);
		init();
	}
	
	public void init(){
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));

		insertArtist = new JPanel();
		initInsertArtist();
		
		insertCity = new JPanel();
		initInsertCity();
		insertArtistInCity = new JPanel();
		initInsertArtistInCity();
		notificationArea = new NotificationArea("Risposta della piattaforma");
		add(notificationArea);
		setKnownCity();
		
	}
	
	public void initInsertArtist(){		
		TitledBorder title;
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		title = BorderFactory.createTitledBorder(
                loweredetched, "Inserimento Artista");
		title.setTitleJustification(TitledBorder.LEFT);
		insertArtist.setBorder(title);
		insertArtist.setLayout(new FlowLayout());
		form = new CustomForm(true);
		for(JComponent x: form.getComponents())
			insertArtist.add(x);


		BinsertArtist = new JButton("Registra Artista");
		BinsertArtist.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String[] ret = form.getData();
				String qu = "insert into artista (nome,cognome,soprannome,nascita,morte) "
							+"values('"+ret[0]+"','"+ret[1]+"','"+ ret[2]+"','"+ret[3]+"','"+ret[4]+"')";
				String msg = db.exec(qu);
				notificationArea.instaMessage(msg);
			}
			
		});
		
		JButton dbg = new JButton("dbg");
		dbg.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String q = "select * from artista;";
				ArrayList<ArrayList<String>> ret = db.query(q);
				for(ArrayList<String> x: ret){
					System.out.println(x.toString());
				}
				
			}
			
		});
		insertArtist.add(BinsertArtist);
		insertArtist.add(dbg);
		
		insertArtist.setPreferredSize(new Dimension(100,200));
		add(insertArtist);

	}
	
	public void initInsertCity(){
		TitledBorder title;
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		title = BorderFactory.createTitledBorder(
                loweredetched, "Inserimento Città");
		title.setTitleJustification(TitledBorder.LEFT);
		insertCity.setBorder(title);
		insertCity.setLayout(new FlowLayout());		
		cityForm  =new CityForm(false);
		insertCity.add(cityForm);
		
		JButton registerArtist = new JButton("Registra");
		registerArtist.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String q  = "INSERT INTO citta (nomeC) values('"+cityForm.getData()[0]+"')";
				String msg = db.exec(q);
				notificationArea.instaMessage(msg);
				if(msg.equals("Successo")){
					setKnownCity();
					cityForm.clear();
				}
			}
			
		});
		insertCity.add(registerArtist);
		JButton dbg  =new JButton("DBG)");
		dbg.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String q = "select * from citta;";
				ArrayList<ArrayList<String>> ret = db.query(q);
				for(ArrayList<String> x: ret){
					System.out.println(x.toString());
				}
				
			}
			
		});
		insertCity.add(dbg);
		insertCity.setPreferredSize(new Dimension(100,200));
		add(insertCity);
	}
	
	public void initInsertArtistInCity(){
		TitledBorder title;
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		title = BorderFactory.createTitledBorder(
                loweredetched, "Inserimento Permanenza di artista in città");
		title.setTitleJustification(TitledBorder.LEFT);
		insertArtistInCity.setBorder(title);
		insertArtistInCity.setLayout(new FlowLayout());		
		artInCity=new CityForm(true);
		form2=new CustomForm(false);
		for(JComponent x: form2.getComponents())
			insertArtistInCity.add(x);
		insertArtistInCity.add(artInCity);
		add(insertArtistInCity);
		JButton b  = new JButton("Registra");
		b.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String[] fvals =form2.getData();
				String[] cvals =  artInCity.getData();
				String q = "insert into sta_in (nomeA,cognomeA,soprannomeA,da,a,citNome)"
						+"values('"+fvals[0]+"','"+fvals[1]+"','"+fvals[2]+"','"+cvals[1]+"','"+cvals[2]+"','"+cvals[0]+"')";
				String msg = db.exec(q);
				notificationArea.instaMessage(msg);
			}
			
		});
		insertArtistInCity.add(b);
		insertArtistInCity.setPreferredSize(new Dimension(100,200));	
	}
	

	public void setKnownCity(){
		String q = "select nomeC from citta";
		ArrayList<ArrayList<String>> ret = db.query(q);
		ArrayList<String>knownCities = new ArrayList<String>();
		for(ArrayList<String> x: ret){
			knownCities.add(x.get(0));
		}
		cityForm.setKnownCities(knownCities);
		artInCity.setKnownCities(knownCities);
	}
	
	
}
