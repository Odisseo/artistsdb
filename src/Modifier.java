import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Modifier  extends JPanel{
	public JTextField text;
	public DbDemon db;
	
	
	
	public Modifier(){
		db=DbDemon.getDb();
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		text = new JTextField(40);
		text.setPreferredSize(new Dimension(100,50));

		add(new JLabel("modifica interna: "));
		add(text);
		JButton perform = new JButton("Esegui");
		
		perform.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				db.exec(text.getText());
			}
			
		});
		add(perform);
			
		
	}
}
