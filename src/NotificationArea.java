import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class NotificationArea extends JPanel{
	JTextArea output;
	
	public NotificationArea(String sTitle){
		TitledBorder title;
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		title = BorderFactory.createTitledBorder(
                loweredetched, sTitle);
		title.setTitleJustification(TitledBorder.LEFT);
		setBorder(title);
		setLayout( new BorderLayout() );
		output=new JTextArea();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		JScrollPane scroll = new JScrollPane(output);
	    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

	    setPreferredSize(new Dimension(100,200));
		add(scroll);
		output.setEditable(false);
	}
	
	public void printlnOutput(String s){
		output.append(s+"\n");
	}
	public void clearOutput(){
		output.setText("");
	}
	public void instaMessage(String s){
		clearOutput();
		printlnOutput(s);
	}
	public void append(String s){
		output.append(s);
	}

}
