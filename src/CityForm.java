import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class CityForm extends JPanel{
	JTextField nome;
	JTextField da;
	JTextField a;
	ArrayList<String> knownCities;
	boolean hasData;
	
	public CityForm(boolean data){
		super();
		JLabel lnome = new JLabel("Nome città*");
		add(lnome);
		nome = new JTextField(10);
		hasData=data;			
		add(nome);
		if(data){
			da= new JTextField(4);
			a= new JTextField(4);
			JLabel lda=new JLabel("da");
			JLabel la=new JLabel("a");
			add(lda);
			add(da);
			add(la);
			add(a);
		}
	}
	
	public String [] getData(){
		if(hasData)
		return new String[]{
				nome.getText(),
				da.getText(),
				a.getText()
		};
		else
			return new String[]{nome.getText()};
	}
	
	public void clear(){
		nome.setText("");
		if(hasData){
			da.setText("");
			a.setText("");
		}
	}
	
	public void setKnownCities(ArrayList<String> arg){
		knownCities=arg;

		nome.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				String ins= nome.getText();
				String sim="";
				if(ins.contains("-")){
					ins=ins.substring(0,ins.lastIndexOf('-'));
					System.out.println("out"+ins);
				}
				if(!ins.equals("")){
					int caret = nome.getCaretPosition();
					sim = SimilarString.like2(ins,knownCities);
					System.out.println(sim);
					nome.setText(ins+sim);
					nome.setCaretPosition(caret);
				}
				else{
					nome.setText("");
				}
			}
			
		});
	}
	

}
