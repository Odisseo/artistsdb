import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Extractor extends JPanel{
	public QueryMake queryMake;
	
	public Extractor(){
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		queryMake=new QueryMake();
		for(Component x: queryMake){
			add(x);
		}
	}

}
