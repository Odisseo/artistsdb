import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class Query3 extends SuperQuery{
	JTextField nome;
	JTextField cognome;
	JTextField soprannome;
	JTextField da;
	JTextField a;
	
	public Query3(){
		init();
	}

	@Override
	public String[] getData() {
		return new String[]{nome.getText(),cognome.getText()};
	}
	
	public void init(){
		JLabel l = new JLabel("Nome di X");
		nome  =new JTextField(10);
		JLabel l2 = new JLabel("Cognome di X");
		cognome  =new JTextField(10);
		JLabel l3 = new JLabel("Soprannome: ");
		soprannome = new JTextField(10);
		JLabel l4 = new JLabel("da: ");
		da= new JTextField(10);
		JLabel l5 = new JLabel("a: ");
		a= new JTextField(10);
		add(l);
		add(nome);
		add(l2);
		add(cognome);
		add(l3);
		add(soprannome);
		add(l4);
		add(da);
		add(l5);
		add(a);
	}
	
	@Override
	public String getQuery(){
		String ret=null;
		String n = nome.getText();
		String c = cognome.getText();
		String s = soprannome.getText();
		String from = da.getText();
		String to = a.getText();
		if(s.equals("")){
			ret="select * from sta_in where nomeA = "+toStar(n)+" and cognomeA="+toStar(c)+" and da>="+toStar(from)+" and a<="+toStar(to)+";";

		}
		else
			ret="select * from sta_in where soprannomeA="+toStar(s)+" and da<="+toStar(from)+" and a>="+toStar(to)+";";
		return ret;
		
	}
	
	@Override
	public String getOutputRow(ArrayList<String> s){
		return s.get(3)+"-"+s.get(4)+" -> "+s.get(5);
	}

}
