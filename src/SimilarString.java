import java.util.ArrayList;

public class SimilarString {

	public static String like(String s, ArrayList<String> arg){
		String ret;
		int index=-1;
		int similar=0;
		for(String x: arg){
			if(index==-1)
				index=0;
			int lunghezza=0;
			int points=0;
			if(s.length()<x.length())
				lunghezza=s.length();
			else
				lunghezza=x.length();
			for(int i = 0; i<lunghezza;i++){
				if(s.charAt(i)==x.charAt(i))
					points++;
			}
			if(similar<points){
				similar=points;
				index=arg.indexOf(x);
			}				
		}
		if(similar==0)
			ret="";
		else{
			if(s.equals(arg.get(index))){
				return "";
			}				
			ret="-"+arg.get(index).substring(s.length()+1);
		
		}
		return ret;
	}
	public static String like2(String s, ArrayList<String> arg){
		String ret="";
		for(String x: arg){
			if(x.contains(s)){
				ret=x;
				break;
			}
		}
		if(! ret.equals("")){
			ret=ret.substring(s.length()+1);
			ret="-"+ret;
		}
		return ret;	
	}
	
}
