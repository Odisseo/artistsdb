import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class Query2 extends SuperQuery{
	JTextField nome;
	JTextField cognome;
	JTextField soprannome;
	
	public Query2(){
		init();
	}

	@Override
	public String[] getData() {
		return new String[]{nome.getText(),cognome.getText()};
	}
	
	public void init(){
		JLabel l = new JLabel("Nome di X");
		nome  =new JTextField(10);
		JLabel l2 = new JLabel("Cognome di X");
		cognome  =new JTextField(10);
		JLabel l3 = new JLabel("Soprannome: ");
		soprannome = new JTextField(10);
		add(l);
		add(nome);
		add(l2);
		add(cognome);
		add(l3);
		add(soprannome);
	}
	
	@Override
	public String getQuery(){
		String ret=null;
		String n = nome.getText();
		String c = cognome.getText();
		String s = soprannome.getText();
		
		if(s.equals("")){
			ret="select * from sta_in where nomeA = "+toStar(n)+" and cognomeA="+toStar(c)+";";

		}
		else
			ret="select * from sta_in where soprannomeA="+toStar(s)+";";
		return ret;
		
	}
	
	@Override
	public String getOutputRow(ArrayList<String> s){
		return s.get(3)+"-"+s.get(4)+" -> "+s.get(5);
	}

}
