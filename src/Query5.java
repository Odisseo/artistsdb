import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class Query5 extends SuperQuery{
	JTextField nomeA;
	JTextField cognomeA;
	JTextField soprannomeA;
	
	JTextField nomeB;
	JTextField cognomeB;
	JTextField soprannomeB;
	
	
	
	public Query5(){
		init();
	}
	
	public void init(){
		JLabel l = new JLabel("Nome di X: ");
		nomeA=new JTextField(10);
		JLabel l1 = new JLabel("Congome di X: ");
		cognomeA= new JTextField(10);
		JLabel l2 = new JLabel("Soprannome di X: ");
		soprannomeA= new JTextField(10);
		
		JLabel l3 = new JLabel("Nome di Y: ");
		nomeB=new JTextField(10);
		JLabel l4 = new JLabel("Congome di Y: ");
		cognomeB= new JTextField(10);
		JLabel l5 = new JLabel("Soprannome di Y: ");
		soprannomeB= new JTextField(10);
		
		add(l);
		add(nomeA);
		add(l1);
		add(cognomeA);
		add(l2);
		add(soprannomeA);
		add(l3);
		add(nomeB);
		add(l4);
		add(cognomeB);
		add(l5);
		add(soprannomeB);
	}
	
	@Override
	public String getQuery(){
		String ret=null;
		String na = nomeA.getText();
		String ca = cognomeA.getText();
		String sa = soprannomeA.getText();
		String nb = nomeB.getText();
		String cb = cognomeB.getText();
		String sb = soprannomeB.getText();
		DbDemon db = DbDemon.getDb();
		boolean abort=false;
		if((na.equals("") || ca.equals(""))){
			ArrayList<ArrayList<String>> value  =db.query("select nome,cognome from artista where soprannome ='"+sa+"';");
			System.out.println(">>>> "+sa+" -- ");

			if(value!=null){
				na=value.get(0).get(0);
				ca=value.get(0).get(1);
				System.out.println(">>>> "+value.get(0).toString());

			}
			else{
				abort=true;
			}
		}
		if((nb.equals("") || cb.equals(""))){
			ArrayList<ArrayList<String>> value  =db.query("select nome,cognome from artista where soprannome ='"+sb+"';");
			if(value!=null){
				nb=value.get(0).get(0);
				cb=value.get(0).get(1);
			}
			else{
				abort=true;
			}
		}
		if(!abort){
		ret="select a.nomeA,a.cognomeA,a.soprannomeA,b.nomeA,b.cognomeA,b.soprannomeA, a.citNome, a.da, a.a"
				+ " from sta_in as a, sta_in as b"
				+ " where a.nomeA='"+na+"' and a.cognomeA='"+ca+"'"
						+ "and b.nomeA='"+nb+"' and b.cognomeA='"+cb+"' and " 
				+ "a.citNome=b.citNome and ((a.da<=b.da and a.a >= b.a) or (a.da>=b.da and a.a<=b.a)) and a.nomeA != b.nomeA;";
		}
		return ret;
		
	}
	
	@Override
	public String getOutputRow(ArrayList<String> s){
		String ret="";
		if(s.get(2).equals("null")){
			ret+=s.get(0)+" "+s.get(1);
		}
		else{
			ret+=s.get(2);
		}
		ret+=" e ";
		if(s.get(5).equals("null"))
			ret+=s.get(3)+" "+s.get(4);
		else
			ret+=s.get(5);
		ret+=" sono stati a "+s.get(6)+" intorno al "+s.get(7)+"-"+s.get(8);
		return ret;
	}

}
