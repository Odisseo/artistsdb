import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class QueryMake extends ArrayList<Component>{
	private final String[] menuQuery={"Domanda da porre","Query Generica","Dove è stato l'artista X?",
			"Dove è stato X tra l'anno Y e Z?",
			"Chi c'era nella città di X tra il Y e il Z?",
			"Dove si potevano incontrare X e Y?"};
	
	JComboBox comboBox;
	JPanel queryContainer;
	JButton makeQuery;
	SuperQuery asker;
	NotificationArea notificationArea;
	DbDemon db;

	
	public QueryMake(){
		initComboBox();
		initQueryContainer();
		makeQuery=new JButton("Avvia interrogazione");
		makeQuery.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){}});
		add(makeQuery);
		notificationArea = new NotificationArea("Risultato Ricerca");
		add(notificationArea);
		db=DbDemon.getDb();
		
	}
	
	public void initNotificationArea(){
		
	}
	
	public void initComboBox(){
		comboBox = new JComboBox(menuQuery);
		comboBox.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				fillForm(comboBox.getSelectedIndex());
			}
			
		});
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		comboBox.setMaximumSize(new Dimension((int)screenSize.getWidth(),20));
		add(comboBox);
	}
	public void initQueryContainer(){
		queryContainer  = new JPanel();
		TitledBorder title;
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		title = BorderFactory.createTitledBorder(
                loweredetched, "Parametri della domanda.");
		title.setTitleJustification(TitledBorder.LEFT);
		queryContainer.setBorder(title);
		queryContainer.setLayout(new FlowLayout());
		JLabel l = new JLabel("Il form per completare la domanda sarà visualizzato qui.");
		queryContainer.add(l);
		queryContainer.setPreferredSize(new Dimension(100,200));
		add(queryContainer);
	}
	
	public void addAllComponentsToForm(){
		for(Component x: queryContainer.getComponents())
			queryContainer.remove(x);
		for(Component x: asker){
			queryContainer.add(x);
		}
		queryContainer.getTopLevelAncestor().revalidate();
		
	}
	
	public void fillForm(int index){
		makeQuery.removeActionListener(makeQuery.getActionListeners()[0]);
		switch(index){
		case 1:
			prepareQuery(new Query1());	
		break;
		case 2:
			prepareQuery(new Query2());	
		break;
		case 3:
			prepareQuery(new Query3());	
		break;
		case 4:
			prepareQuery(new Query4());	
		break;
		case 0:
		case 5:
			prepareQuery(new Query5());	
		break;
		
		
		}	
	}
	
	public void makeQueryAndPrint(String s,SuperQuery ask){
		if(s!=null){
			ArrayList<ArrayList<String>> res =db.query(s);
			notificationArea.clearOutput();
			for(ArrayList<String> x: res){
				notificationArea.append(ask.getOutputRow(x)+"\n");
			}
		}
	}
	
	public void prepareQuery(SuperQuery q){
		asker = q;
		addAllComponentsToForm();
		makeQuery.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(asker.getQuery());
				makeQueryAndPrint(asker.getQuery(),asker);
			}
		});		
	}

}
