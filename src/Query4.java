import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class Query4 extends SuperQuery{
	JTextField citta;
	JTextField da;
	JTextField a;
	
	public Query4(){
		init();
	}
	
	public void init(){
		JLabel l = new JLabel("Città: ");
		citta=new JTextField(10);
		JLabel l4 = new JLabel("da: ");
		da= new JTextField(10);
		JLabel l5 = new JLabel("a: ");
		a= new JTextField(10);
		add(l);
		add(citta);
		add(l4);
		add(da);
		add(l5);
		add(a);
	}
	
	@Override
	public String getQuery(){
		String ret=null;
		String n = citta.getText();
		String from = da.getText();
		String to = a.getText();
		ret="select * from sta_in where citNome="+toStar(n)+" and da<="+toStar(from)+" and a>="+toStar(to)+";";
		return ret;
		
	}
	
	@Override
	public String getOutputRow(ArrayList<String> s){
		return s.get(0)+" "+s.get(1)+" "+s.get(2);
	}

}
