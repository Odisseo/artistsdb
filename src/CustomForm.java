import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CustomForm{
	JTextField nome;
	JTextField soprannome;
	JTextField cognome;
	JTextField nascita;
	JTextField morte;
	ArrayList<JComponent> components ;
	final int IGNORANZA=10;
	boolean mode;
	
	public CustomForm(boolean m){
		super();
		mode=m;
		init();
	}
	
	public ArrayList<JComponent> getComponents(){
		return components;
	}
	
	
	public void init(){
		components=new ArrayList<JComponent>();
		soprannome= new JTextField(IGNORANZA);
		JLabel lnn = new JLabel("Nome d'arte:");
		components.add(lnn);
		components.add(soprannome);

		
		nome= new JTextField(IGNORANZA);
		JLabel ln = new JLabel("Nome*:");
		components.add(ln);
		components.add(nome);
		
		cognome= new JTextField(IGNORANZA);
		JLabel lc = new JLabel("Cognome*:");
		components.add(lc);
		components.add(cognome);
		
		if(mode){
		nascita= new JTextField(IGNORANZA);
		JLabel dn = new JLabel("data di\n nascita:");
		components.add(dn);
		components.add(nascita);
		
		morte= new JTextField(IGNORANZA);
		JLabel dm = new JLabel("data di\n morte:");
		components.add(dm);
		components.add(morte);
		}

	}
	
	public String[] getData(){ 
		if(mode)
		return new String[]{
				 cleanInput(nome.getText()),
				 cleanInput(cognome.getText()),
				 cleanInput(soprannome.getText()),
				 cleanInput(nascita.getText()),
				 cleanInput(morte.getText())
		};
		else{
			return new String[]{
					 cleanInput(nome.getText()),
					 cleanInput(cognome.getText()),
					 cleanInput(soprannome.getText()),

			};
		}
	}
	public void clear(){
		nome.setText("");
		cognome.setText("");
		soprannome.setText("");
		if(mode){
			nascita.setText("");
			morte.setText("");
		}
	}
	
	public String cleanInput(String in){
		String ret=in;
		if(in.equals(""))
			ret=null;
		return ret;
	}	

}
