import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.ScrollPaneLayout;

import sun.awt.X11.Screen;

public class MainFrame extends JFrame{
	public Selector selector;

	public JPanel area;
	public Insertor insertor;
	public Extractor extractor;
	public Modifier modifier;
	
	public MainFrame(){
		super();
		this.setLayout(new BoxLayout(this.getContentPane(),BoxLayout.Y_AXIS));
		selector = new Selector(this);
		add(selector);
		extractor = new Extractor();
		insertor  =new Insertor();		
		modifier = new Modifier();

		pack();
		setVisible(true);
		this.setSize(new Dimension(600,600));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	
	public void setInsertPanel(){
		if(area!=null){
			this.remove(area);
		}
		add(insertor);
		area = insertor;	
		revalidate();
		repaint();
	}
	
	public void setExtractPanel(){
		if(area!=null){
			this.remove(area);
		}
		add(extractor);
		area = extractor;	
		revalidate();
		repaint();
	}
	
	public void setModifierPanel(){
		if(area!=null){
			this.remove(area);
		}
		add(modifier);
		area = modifier;	
		revalidate();
		repaint();		
	}
	
	public class Selector extends JPanel{
		JButton insert;
		JButton search;
		JButton modif;
		MainFrame main;
		
		public Selector(MainFrame m){
			main=m;
			setLayout(new FlowLayout());
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			this.setMaximumSize(new Dimension((int) screenSize.getWidth(),100) );			
			init();
			
		}
		
		public void init(){
			insert = new JButton("Inserimento");
			
			insert.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					main.setInsertPanel();
				}
			});
			
			search = new JButton("Ricerca");
			
			search.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					main.setExtractPanel();
				}
			});
			
			modif = new JButton("Manutenzione");
			modif.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					main.setModifierPanel();
				}
				
			});
			
			add(insert);
			add(search);
			add(modif);
		}
	}
	

	


}
